"use strict";
const chromium = require("chrome-aws-lambda");
var AWS = require("aws-sdk");
const fs = require("fs");
// const path = require("path");

AWS.config.update({ region: "us-east-1" });
const s3 = new AWS.S3();

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
};

module.exports.imageExport = async (event, context, callBack) => {


  const executablePath =  event.isOffline
    ? "./node_modules/puppeteer/.local-chromium/mac-756035/chrome-mac/Chromium.app/Contents/MacOS/Chromium"
    : await chromium.executablePath;


  let browser = null;

  

  try {
    browser = await chromium.puppeteer.launch({
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      executablePath,
      headless: chromium.headless 
    });

    const page = await browser.newPage();

    await page.setViewport({
      width: 1920,
      height: 1080,
      isMobile: false
    });

    const url = event.queryStringParameters.url
    await page.goto(url, {
      waitUntil: ["domcontentloaded", "networkidle2"]
    })
      page.content()
    let documentPath = event.isOffline
      ? ".tmp/test.jpg"
      : "/tmp/test.jpg"
    await page.screenshot({
      path: documentPath, type: 'jpeg', quality: 100
    });

    // const content = await page.content()
    // let pathToFile = path.resolve(__dirname, ".tmp/test.jpg")
    const file = fs.readFileSync(documentPath)
    const buffer = await Buffer.from(file).toString('base64')

    const response = {
      headers: {
        "Content-Type": "image/jpeg",
        "Accept": "image/jpeg"

      },
      statusCode: 200,
      body: buffer,
      isBase64Encoded: true
    };


    return callBack(null, response)

  } catch (error) {
    return context.fail(error);
  } finally {
    if (browser !== null) {
      await browser.close();
    }
  }
};